<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		String izena = request.getParameter("izena");
		String abizena = request.getParameter("abizena");
		String erabiltzailea = request.getParameter("erabiltzailea");
		String nan = request.getParameter("nan");
		String tzenbakia = request.getParameter("tzenbakia");
		String pasahitza = request.getParameter("pasahitza");
		String pasahitza2 = request.getParameter("pasahitza2");
		erabiltzaileaModel erabil = new erabiltzaileaModel();

		if (pasahitza.equals(pasahitza2) == true) {

			// If honek erabiltzailea lehendik existitzen den konprobatzen du
			if (erabil.erabiltzaileaKonprobatu(erabiltzailea) == true) {

				//Errore mezua bidaltzen du
				String errorea = "Erabiltzailea datu basean existitzen da";
				request.setAttribute("erroremezua", errorea);

				// Errorea eta gero zein orrialdera joango den
				String orrialdera = "../view/erregistratu.jsp";
				request.setAttribute("orrialdera", orrialdera);
				request.getRequestDispatcher("../view/error.jsp").forward(request, response);
			} else {
				erabil.erregistratu(izena, abizena, erabiltzailea, nan, tzenbakia, pasahitza);

				String confirm = "Datu basean erregistratu zara.";
				request.setAttribute("confirmmezua", confirm);

				// Errorea eta gero zein orrialdera joango den
				String orrialdera = "../index.jsp";
				request.setAttribute("orrialdera", orrialdera);
				request.getRequestDispatcher("../view/confirm.jsp").forward(request, response);
			}
		}else{
			String errorea = "Sartutako bi pasahitzak desberdinak ziren. Berriro saiatu.";
			request.setAttribute("erroremezua", errorea);
			String orrialdera = "../view/erregistratu.jsp";
			request.setAttribute("orrialdera", orrialdera);
			request.getRequestDispatcher("../view/error.jsp").forward(request, response);
		}
	%>
</body>
</html>