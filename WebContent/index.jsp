<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<%if (session.getAttribute("erabiltzailea") != null) // checks if there's an active session
{
	response.sendRedirect("controller/homeController.jsp");
}  %>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/bootstrap.min.js"></script>
</head>

<body>
  <video autoplay="autoplay" loop="loop" muted="muted" poster="screenshot.jpg" id="background">
      <source src="videos/videoplayback.mp4">
    </video>

  <div class="card card-container">

    <img id="profile-img" class="profile-img-card" src="images/avatar.png" />
    <p id="profile-name" class="profile-name-card"></p>
    <form class="form-signin" method="get" action="controller/loginController.jsp">
      <span id="reauth-email" class="reauth-email"></span>
      <input type="text" id="inputEmail" class="form-control" placeholder="Erabiltzailea" name="erabiltzailea" required autofocus>
      <input type="password" id="inputPassword" class="form-control" placeholder="Pasahitza" name= "pasahitza" required>
      <div id="remember" class="checkbox">
        <label>
                          <input type="checkbox" value="remember-me"> Gogoratu
                      </label>
      </div>
      <button class="btn btn-lg btn-success btn-block btn-signin" type="submit">Saioa Hasi</button>
    </form>
    <!-- /form -->
    <a href="view/erregistratu.jsp" class="forgot-password">
                  Konturik ez?
              </a>
  </div>
  <!-- /card-container -->
  <!-- /container -->
</body>


</html>
