<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html lang="en">
<%
	if (session.getAttribute("erabiltzailea") == null) // checks if there's an active session
	{
		response.sendRedirect("../index.jsp");
	}
	ArrayList<artikuloaClass> artikuloak = (ArrayList<artikuloaClass>) request.getAttribute("artikuloak");

%>
<head>

 <meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<script src="../js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script src="../js/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script src="../js/bootstrap.js" crossorigin="anonymous"></script>

<title>DENDA</title>

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../css/home.css" rel="stylesheet">

</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
		<div class="container">
			<a class="navbar-brand" href="#">Gure denda</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarResponsive" aria-controls="navbarResponsive"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active"><a class="nav-link" href="#">Home
							<span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="#">About</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Services</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="#">Contact</a>
					</li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"><span style="color:#75ad6f;text-transform:capitalize">Zure kontua: <%=session.getAttribute("erabiltzailea")%></span>  </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<h6 class="dropdown-header">Administrazio aukerak</h6>
							<%
								String rango;
								rango = (String) session.getAttribute("rango");
								if (rango.equalsIgnoreCase("1")) {
							%><a class="dropdown-item"
								href="../controller/artikuloBerriaController.jsp?aukera=1">Artikulo
								berria</a>
							<%
								}
							%>
							<a class="dropdown-item" href="#">Artikulo bat aldatu</a>
							<a class="dropdown-item" href="#">Artikulo bat ezabatu</a>

							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">Bezeroak ikusi</a>
							<a class="dropdown-item" href="#">Bezero bat aldatu</a>
							<a class="dropdown-item" href="#">Bezero bat ezabatu</a>
	<div class="dropdown-divider"></div>
	<a class="dropdown-item" href="#">Salmenta guztiak ikusi</a>


							<div class="dropdown-divider"></div>
							 <h6 class="dropdown-header">Kontuaren Ezarpenak</h6>
							<a class="dropdown-item" style="background-color:#ffb8b8" href="mainController.jsp?aukera=1">Saioa Itxi</a>
						</div></li>
				</ul>
			</div>
		</div>
	</nav>

	<!-- Page Content -->
	<div class="container">

		<div class="row">

			<div class="col-lg-3">

				<h1 class="my-4">Gure denda</h1>
				<div class="list-group">
					<a href="#" class="list-group-item">Category 1</a> <a href="#"
						class="list-group-item">Category 2</a> <a href="#"
						class="list-group-item">Category 3</a>
				</div>

			</div>
			<!-- /.col-lg-3 -->

			<div class="col-lg-9">

				<div id="carouselExampleIndicators" class="carousel slide my-4"
					data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carouselExampleIndicators" data-slide-to="0"
							class="active"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner" role="listbox">
						<div class="carousel-item active">
							<img class="d-block img-fluid" src="http://placehold.it/900x350"
								alt="First slide">
						</div>
						<div class="carousel-item">
							<img class="d-block img-fluid" src="http://placehold.it/900x350"
								alt="Second slide">
						</div>
						<div class="carousel-item">
							<img class="d-block img-fluid" src="http://placehold.it/900x350"
								alt="Third slide">
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators"
						role="button" data-slide="prev"> <span
						class="carousel-control-prev-icon" aria-hidden="true"></span> <span
						class="sr-only">Previous</span>
					</a> <a class="carousel-control-next" href="#carouselExampleIndicators"
						role="button" data-slide="next"> <span
						class="carousel-control-next-icon" aria-hidden="true"></span> <span
						class="sr-only">Next</span>
					</a>
				</div>

				<div class="row">


					<%
						for (int i = 0; i < artikuloak.size(); i++) {
					%>
					<div class="col-lg-4 col-md-6 mb-4">
						<div class="card h-100">
							<a href="#"><img class="card-img-top"
								src="http://placehold.it/700x400" alt=""></a>
							<div class="card-body">
								<h4 class="card-title">
									<a href="#"><%=artikuloak.get(i).getIzena()%></a>
								</h4>
								<h5><%=artikuloak.get(i).getPrezioa()%> EUR</h5>
								<p class="card-text"><%=artikuloak.get(i).getDeskribapena()%></p>
							</div>
							<div class="card-footer">
								<small class="text-muted"><button type="button" class="btn">Gehitu</button></small>
							</div>
						</div>
					</div>
					<%
						}
					%>









				</div>
				<!-- /.row -->

			</div>
			<!-- /.col-lg-9 -->

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->

	<!-- Footer -->
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy; Your
				Website 2017</p>
		</div>
		<!-- /.container -->
	</footer>


</body>

</html>
