<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" type="text/css" href="../css/erregistratu.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <script src="../js/bootstrap.min.js"></script>
</head>

<body>
  <video autoplay="autoplay" loop="loop" muted="muted" poster="screenshot.jpg" id="background">
      <source src="../videos/videoplayback.mp4">
    </video>

  <div class="card card-container">

    <label for="card-container">ERREGISTROA</label>
    <p id="profile-name" class="profile-name-card"></p>
    <form class="form-signin" action="../controller/erregistroController.jsp">
      <span id="reauth-email" class="reauth-email"></span>
      <label for="inputIzena">Izena</label>
      <input type="text" id="inputIzena" name="izena" class="form-control" placeholder="Izena" maxlength="20" required autofocus>
      <label for="inputAbizena">Abizena</label>
      <input type="text" id="inputAbizena" name="abizena" class="form-control" placeholder="Abizena" maxlength="50" required autofocus>
      <label for="inputErabiltzailea">Erabiltzailea</label>
      <input type="text" id="inputErabiltzailea" name="erabiltzailea" class="form-control" placeholder="Erabiltzailea" maxlength="20" required autofocus>
      <label for="inputdni">NAN</label>
      <input type="text" id="inputdni" name="nan" class="form-control" placeholder="NAN" maxlength="9" required autofocus>
      <label for="inputTZenbakia">Telefono zenbakia</label>
      <input type="text" id="inputTZenbakia" class="form-control" name="tzenbakia" placeholder="adib.: 687514587" pattern="\d{9}" maxlength="9" required autofocus>
      <label for="inputPassword ">Pasahitza </label>
      <label for="inputPassword" class="karaktere"> (+6 karaktere)</label>
      <input type="password" id="inputPassword " name="pasahitza" class="form-control " placeholder="Pasahitza" pattern=".{6,}" maxlength="30" required>
      <br>
      <input type="password" id="inputPassword2 " name="pasahitza2" class="form-control " placeholder="Pasahitza berriz" pattern=".{6,}" maxlength="30" required>
      <br>

      <button class="btn btn-lg btn-success btn-block btn-signin " type="submit ">Erregistratu</button>
    </form>
    <!-- /form -->
  </div>

</body>
</html>