package model;

public class erabiltzaileaClass extends Connect {

	// ATRIBUTOAK

	protected int idErabiltzaile;
	protected String izena;
	protected String erabiltzailea;
	protected String abizena;
	protected String pasahitza;
	protected String dni;
	protected String tzenbakia;
	protected String rango;

	// PARAMETRODUN KONSTRUKTOREA

	public erabiltzaileaClass(int idErabiltzaile, String izena, String erabiltzailea, String abizena, String pasahitza,
			String dni, String tzenbakia,String rango) {
		super();
		this.idErabiltzaile = idErabiltzaile;
		this.izena = izena;
		this.erabiltzailea = erabiltzailea;
		this.abizena = abizena;
		this.pasahitza = pasahitza;
		this.dni = dni;
		this.tzenbakia = tzenbakia;
	}

	// PARAMETRORIK GABEKO KONSTRUKTOREA

	public erabiltzaileaClass() {
		super();
	}

	// GETTER ETA SETTERRAK

	public int getIdErabiltzaile() {
		return idErabiltzaile;
	}

	public void setIdErabiltzaile(int idErabiltzaile) {
		this.idErabiltzaile = idErabiltzaile;
	}

	public String getIzena() {
		return izena;
	}

	public void setIzena(String izena) {
		this.izena = izena;
	}

	public String getAbizena() {
		return abizena;
	}

	public void setAbizena(String abizena) {
		this.abizena = abizena;
	}

	public String getPasahitza() {
		return pasahitza;
	}

	public void setPasahitza(String pasahitza) {
		this.pasahitza = pasahitza;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getTzenbakia() {
		return tzenbakia;
	}

	public void setTzenbakia(String tzenbakia) {
		this.tzenbakia = tzenbakia;
	}

	public String getErabiltzailea() {
		return erabiltzailea;
	}

	public void setErabiltzailea(String erabiltzailea) {
		this.erabiltzailea = erabiltzailea;
	}
	public String getRango() {
		return rango;
	}
	
	public void setRango(String rango) {
		this.rango = rango;
	}

}
