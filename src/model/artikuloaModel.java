package model;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class artikuloaModel extends artikuloaClass {

	// KONSTRUKTOREAK

	public artikuloaModel() {
		super();
	}

	public artikuloaModel(int idArtikuloa, String izena, String deskribapena, String mota, double prezioa,
			String neurria,int Stock) {
		super(idArtikuloa, izena, deskribapena, mota, prezioa, neurria,Stock);
	}

	// METODOAK

	public boolean artikuloBerria(int pmota, int pneurria, String pizena, String pdeskribapena, double pprezioa) {
		boolean amaituta = false;

		// Prozedura hasten da
		String SQL = "CALL 	spAgregarArticulo(?,?,?,?,?)";
		try (Connection connection = Connect.getConnection(); CallableStatement cs = connection.prepareCall(SQL)) {
			cs.setString(1, pizena);
			cs.setString(2, pdeskribapena);
			cs.setInt(3, pmota);
			cs.setDouble(4, pprezioa);
			cs.setInt(5, pneurria);
			cs.execute();
			amaituta = true;

		} catch (Exception e) {
			System.out.println(e);
		}

		return amaituta;
	}
	
	public ArrayList<artikuloaClass> getArtikuloak() throws SQLException {
		ArrayList<artikuloaClass> art_guztiak = new ArrayList<artikuloaClass>();
		
		Statement st = this.getConnection().createStatement(); // the connection variable
		ResultSet rs = st.executeQuery("CALL spListadoArticulosEnVenta");

		while (rs.next()) {
			int id = Integer.parseInt((rs.getString(1)));
			String izena = (rs.getString(2));
			String deskribapena = (rs.getString(3));
			String neurria = (rs.getString(4));
			double prezioa = Double.parseDouble(rs.getString(5));
			int stock = Integer.parseInt((rs.getString(6)));
			String mota = (rs.getString(7));
			
			artikuloaClass artikulo01 = new artikuloaClass();
			artikulo01.idArtikuloa = id;
			artikulo01.izena = izena;
			artikulo01.deskribapena = deskribapena;
			artikulo01.mota = mota;
			artikulo01.prezioa = prezioa;
			artikulo01.neurria = neurria;
			artikulo01.Stock = stock;

			art_guztiak.add(artikulo01);

		}
		return art_guztiak;
	}
}