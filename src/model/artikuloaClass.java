package model;

public class artikuloaClass extends Connect{

	protected int idArtikuloa;
	protected String izena;
	protected String deskribapena;
	protected String mota;
	protected double prezioa;
	protected String neurria;
	protected int Stock;
	
	//Konstruktoreak
	public artikuloaClass(int idArtikuloa, String izena, String deskribapena, String mota, double prezioa,
			String neurria,int Stock) {
		super();
		this.idArtikuloa = idArtikuloa;
		this.izena = izena;
		this.deskribapena = deskribapena;
		this.mota = mota;
		this.prezioa = prezioa;
		this.neurria = neurria;
		this.Stock = Stock;
	}
	
	// GETTER ETA SETTERRAK

	public artikuloaClass() {
		super();
	}

	public int getIdArtikuloa() {
		return idArtikuloa;
	}

	public void setIdArtikuloa(int idArtikuloa) {
		this.idArtikuloa = idArtikuloa;
	}

	public String getIzena() {
		return izena;
	}

	public void setIzena(String izena) {
		this.izena = izena;
	}

	public String getDeskribapena() {
		return deskribapena;
	}

	public void setDeskribapena(String deskribapena) {
		this.deskribapena = deskribapena;
	}

	public String getMota() {
		return mota;
	}

	public void setMota(String mota) {
		this.mota = mota;
	}

	public double getPrezioa() {
		return prezioa;
	}

	public void setPrezioa(double prezioa) {
		this.prezioa = prezioa;
	}

	public String getNeurria() {
		return neurria;
	}

	public void setNeurria(String neurria) {
		this.neurria = neurria;
	}
	public int getStock() {
		return Stock;
	}
	
	public void setStock(int Stock) {
		this.Stock = Stock;
	}
	
	
	
	
	
}
