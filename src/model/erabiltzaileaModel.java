package model;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class erabiltzaileaModel extends erabiltzaileaClass {

	// Super konstruktorea

	public erabiltzaileaModel() {
		super();
	}

	public erabiltzaileaModel(int idErabiltzaile, String izena, String erabiltzailea, String abizena, String pasahitza,
			String dni, String tzenbakia,String rango) {
		super(idErabiltzaile, izena, erabiltzailea, abizena, pasahitza, dni, tzenbakia,rango);
	}

	// METODOAK

	// Pasahitza enkriptatzeko metodoa.
	public String hashPassword(String plainTextPassword) {
		String securePasahitza = BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
		return securePasahitza;
	}

	// Sartutako pasahitza eta datubaseko hash pasahitza konparatzen ditu
	public boolean checkPass(String plainPassword, String erabiltzailea) {
		boolean login = false;
		String hashedPassword = hartuPasahitza(erabiltzailea);
		if (BCrypt.checkpw(plainPassword, hashedPassword)) {
			login = true;
		}
		return login;
	}

	// Esandako erabiltzailearen hash pasahitza hartzen du datubasetik
	public String hartuPasahitza(String perabiltzailea) {
		String SQL = "CALL 	spCogerContrasena(?,?)";
		String emaitza = "";
		try (Connection connection = Connect.getConnection(); CallableStatement cs = connection.prepareCall(SQL)) {
			cs.setString(1, perabiltzailea);
			cs.execute();
			emaitza = cs.getString(2);
		} catch (Exception e) {
			System.out.println(e);
		}
		return emaitza;
	}

	// Erregistratu baino lehen erabiltzailea existitzen den ala ez konprobatu
	public boolean erabiltzaileaKonprobatu(String perabiltzailea) {
		boolean aurkituta = false;

		String SQL = "CALL 	spComprobarSiExisteUsuario(?,?)";
		try (Connection connection = Connect.getConnection(); CallableStatement cs = connection.prepareCall(SQL)) {
			cs.setString(1, perabiltzailea);
			cs.execute();
			String emaitza = cs.getString(2);
			if (emaitza.equalsIgnoreCase(perabiltzailea)) {
				aurkituta = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return aurkituta;
	}

	// Bezero bat erregistratzen du
	public boolean erregistratu(String pizena, String pabizena, String perabiltzailea, String pnan, String pzenbakia,
			String ppasahitza) {
		boolean amaituta = false;
		String pasahitzaEnkriptatuta = "";
		// Pasahitza enkriptatzen da hash moduan
		pasahitzaEnkriptatuta = hashPassword(ppasahitza);

		// Prozedura hasten da
		String SQL = "CALL 	spRegistrarUsuario(?,?,?,?,?,?)";
		try (Connection connection = Connect.getConnection(); CallableStatement cs = connection.prepareCall(SQL)) {
			cs.setString(1, pizena);
			cs.setString(2, pabizena);
			cs.setString(3, perabiltzailea);
			cs.setString(4, pnan);
			cs.setString(5, pzenbakia);
			cs.setString(6, pasahitzaEnkriptatuta);
			cs.execute();
			amaituta = true;
		} catch (Exception e) {
			System.out.println(e);
		}
		return amaituta;
	}
	public String HartuRango(String perabiltzailea) {
		String SQL = "CALL 	spObtenerRango(?,?)";
		String rango="";
		try (Connection connection = Connect.getConnection(); CallableStatement cs = connection.prepareCall(SQL)) {
			cs.setString(1, perabiltzailea);
			cs.execute();
rango=cs.getString(2);
		} catch (Exception e) {
			System.out.println(e);
		}
		return rango;
	}


}
