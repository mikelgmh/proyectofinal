package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class artikuloMotakModel extends artikuloMotakClass{

	public artikuloMotakModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ArrayList<artikuloMotakClass> Getmotak() throws SQLException {
		ArrayList<artikuloMotakClass> motak = new ArrayList<artikuloMotakClass>();
		
		Statement st = this.getConnection().createStatement(); // the connection variable
		ResultSet rs = st.executeQuery("CALL spTiposArtículo");

		while (rs.next()) {
			int id = Integer.parseInt((rs.getString(1)));
			String izena = (rs.getString(2));
			artikuloMotakClass ola = new artikuloMotakClass();
			ola.idMota = id;
			ola.izena = izena;

			motak.add(ola);

		}
		return motak;
	}
}
